(function () {
    class MiModal {
        constructor() {
            this.defaults = {
                modalSelector: ".mi-modal",
                iframeSelector: ".mi-modal__iframe",
                modalContentSelector: ".mi-modal__content",
                selector: "[data-toggle='mi-modal']",
                closeSelector: "[data-toggle='mi-modal-close']",
                youtube: {
                    matcher: /(youtube\.com|youtu\.be|youtube\-nocookie\.com)\/(watch\?(.*&)?v=|v\/|u\/|embed\/?)?(videoseries\?list=(.*)|[\w-]{11}|\?listType=(.*)&list=(.*))(.*)/i,
                    matcherWidth: /<width>(.*)<\/width>/i,
                    matcherHeight: /<height>(.*)<\/height>/i,
                    url: "https://www.youtube.com/embed/$4",
                    thumb: "https://img.youtube.com/vi/$4/hqdefault.jpg",
                    xml: "https://www.youtube.com/oembed?url=https://www.youtube.com/watch?v=$4&format=xml",
                },
            }
            this.body = document.body;

            this.init()
        }

        getIframeHref(youtubeHref) {
            let matchResult = youtubeHref.match(this.defaults.youtube.matcher);
            let iframeHref = this.defaults.youtube.url.replace("$4", matchResult[4])
            return iframeHref;
        }

        setIframeSize() {
            let height, width, k = 1;
            let modalContent = document.querySelector(this.defaults.modalContentSelector);
            let iframe = document.querySelector(this.defaults.iframeSelector);
            if (modalContent && iframe) {
                let iframeWidth = iframe.clientWidth;
                let iframeHeight = iframe.clientHeight;
                let iframeRatio = iframeWidth / iframeHeight;
                let modalContentWidth = modalContent.clientWidth;
                let modalContentHeight = modalContent.clientHeight;
                if (iframeWidth > iframeHeight) {
                    width = modalContentWidth;
                    height = (modalContentWidth / iframeRatio);

                    if (height > modalContentHeight) {
                        k = modalContentHeight / height;
                    }

                    iframe.style.width = width * k + 'px';
                    iframe.style.height = height * k + 'px';
                } else {
                    width = (modalContentHeight * iframeRatio);
                    height = modalContentHeight;
                    if (width > modalContentWidth) {
                        k = modalContentWidth / width;
                    }
                    iframe.style.width = width * k + 'px';
                    iframe.style.height = height * k + 'px';
                }
                iframe.classList.add('is-show')
            }
        }

        addModalFocus() {

            // add all the elements inside modal which you want to make focusable
            const  focusableElements =
                'a[href], area[href], input:not([disabled]):not([type="hidden"]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable]';
            const modal = document.querySelector(this.defaults.modalSelector); // select the modal by it's id

            const firstFocusableElement = modal.querySelectorAll(focusableElements)[0]; // get first element to be focused inside modal
            const focusableContent = modal.querySelectorAll(focusableElements);
            const lastFocusableElement = focusableContent[focusableContent.length - 1]; // get last element to be focused inside modal

            this.handlerModalTab = function(e) {
                console.log(1)
                let isTabPressed = e.key === 'Tab' || e.keyCode === 9;

                if (!isTabPressed) {
                    return;
                }

                if (e.shiftKey) { // if shift key pressed for shift + tab combination
                    if (document.activeElement === firstFocusableElement) {
                        lastFocusableElement.focus(); // add focus for the last focusable element
                        e.preventDefault();
                    }
                } else { // if tab key is pressed
                    if (document.activeElement === lastFocusableElement) { // if focused has reached to last focusable element then focus first focusable element after pressing tab
                        firstFocusableElement.focus(); // add focus for the first focusable element
                        e.preventDefault();
                    }
                }
            }
            document.addEventListener('keydown', this.handlerModalTab);
        }

        addModal(youtubeHref) {
            let iframeHref = this.getIframeHref(youtubeHref);
            this.body.insertAdjacentHTML('afterend', `<div class="mi-modal" role="dialog" tabindex="-1">
                <div class="mi-modal__overlay"></div>
                <div class="mi-modal__container">
                    <div class="mi-modal__content">
                        <div class="mi-modal__iframe-wrap">
                            <iframe src="${iframeHref}" class="mi-modal__iframe"></iframe>
                            <button type="button" class="mi-modal__close" data-togle="mi-modal-close" title="Close">Close</button>
                        </div>
                    </div>
                </div>
            </div>`)
            let iframe = document.querySelector(".mi-modal__iframe");
            let modalClose = document.querySelector(".mi-modal__close");
            let modalContainer = document.querySelector(".mi-modal__container");

            iframe.addEventListener('load', this.setIframeSize.bind(this));
            modalClose.addEventListener('click', this.removeModal.bind(this));
            modalContainer.addEventListener('click', this.removeModal.bind(this));
            this.addModalFocus();
        }

        removeModal() {
            let modal = document.querySelector(this.defaults.modalSelector);
            if (modal) {
                this.eventsOff();
                modal.remove();
            }
        }

        destroy() {
            this.eventsOff();
            document.removeEventListener("click", this.handlerTriggerClick);
        }

        eventsOff() {
            let iframe = document.querySelector(".mi-modal__iframe");
            let modalClose = document.querySelector(".mi-modal__close");
            let modalContainer = document.querySelector(".mi-modal__container");
            iframe.removeEventListener('load', this.setIframeSize);
            modalClose.removeEventListener('click', this.removeModal);
            modalContainer.removeEventListener('click', this.removeModal);
            document.removeEventListener('keydown', this.handlerModalTab);
            window.removeEventListener('resize', this.handlerResize);
        }

        handlerResize() {
            console.log(1)
            let modal = document.querySelector(this.defaults.modalSelector);
            modal && this.setIframeSize()
        }

        eventsOn() {
            let _this = this;

            this.handlerTriggerClick = function(e) {
                for (let target = e.target; target && target != this; target = target.parentNode) {
                    if (target.matches(_this.defaults.selector)) {
                        (function () {
                            e.preventDefault();
                            let youtubeHref = this.getAttribute("href");
                            _this.addModal(youtubeHref)

                        }).call(target, e);
                        break;
                    }
                }
            }

            document.addEventListener("click", this.handlerTriggerClick, false);
            window.addEventListener('resize', this.handlerResize.bind(this), true);
        }

        init() {
            this.eventsOn();
        }
    }

    var miModal = new MiModal();
})()